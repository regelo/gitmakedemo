#include <stdio.h>
#include <stdlib.h>
#include "data.h"

int main(int argc, char *argv[]) {
  
  // Si on n'a pas d'argument on retourne une erreur
  if (argc < 2) return -1;
  
  int index = atoi(argv[1]);
  
  // Obtenir les données liées à l'argument fourni.
  int data = get_data (index);
  
  // Affichage du résultat.
  printf("Résultat = %d  \n", data);
  
  // Fin du logiciel sans erreur.
  return 0;
}