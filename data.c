#include "data.h"

int get_data(int index) {
    
  switch(index) {
    case 0: return 0; break;
    case 1: return 100; break;
    case 2: return 400; break;
    case 3: return 800; break;
    case 4: return 5678; break;
    case 5: return 998; break;
    default: return -2; break;
  }
}
